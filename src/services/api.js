import axios from "axios";
import { apiHost } from "../config";

import { logError } from "./logger";

class Api {
  constructor(baseURL) {
    this.adapter = axios.create({
      baseURL,
    });
  }

  request = async (url, type = "get", payload, headers = {}) => {
    try {
      const response = await this.adapter.request({
        url,
        method: type,
        data: payload,
        headers,
      });
      return Promise.resolve(response.data);
    } catch (err) {
      return Promise.reject(logError(err, "api"));
    }
  };
}

export default new Api(apiHost);
