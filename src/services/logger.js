import { env } from "../config";

export const logError = (err, level) => {
  if (env === "dev" && !err.level) {
    console.error(`${err.level || level} error: ${err.message}`);
    console.error(err);
  }
  if (err.level) return err;
  else return { ...err, level };
};
