import React from "react";

import UpdateRecipeForm from "../../containers/forms/update-recipe-form";

const UpdateRecipePage = props => <UpdateRecipeForm />;

export default UpdateRecipePage;
