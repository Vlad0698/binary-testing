import RecipesPage from "./recipes";
import AddRecipePage from "./add-recipe";
import SingleRecipePage from "./single-recipe";
import UpdateRecipePage from "./update-recipe";

const pages = {
  RecipesPage,
  AddRecipePage,
  SingleRecipePage,
  UpdateRecipePage,
};

export default pages;
