import React from "react";

import AddRecipeForm from "../../containers/forms/add-recipe-form";

const AddRecipePage = props => <AddRecipeForm {...props} />;

export default AddRecipePage;
