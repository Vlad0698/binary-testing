import React from "react";

import RecipesList from "../../containers/recipes-list";

const RecipesPage = props => <RecipesList />;

export default RecipesPage;
