import React from "react";

import Recipe from "../../containers/recipe";
import RecipeHistory from "../../components/recipe-history";

const SingleRecipePage = props => (
  <>
    <Recipe />
    <RecipeHistory />
  </>
);

export default SingleRecipePage;
