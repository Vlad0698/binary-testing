import React, { useState, useEffect } from "react";
import "./styles.sass";
import Burger from "../../assets/icons/burger.png";
import Close from "../../assets/icons/close.png";

import IconButton from "../basic/icon-button";
import MenuItem from "./MenuItem";
import { withRouter } from "react-router-dom";

const Menu = ({ location: { pathname } }) => {
  const [opened, setOpened] = useState(false);

  useEffect(() => {
    setOpened(false);
  }, [pathname]);

  return (
    <>
      <aside className={`menu${opened ? " menu--opened" : ""}`}>
        <IconButton
          className="menu__burger-btn"
          icon={opened ? Close : Burger}
          onClick={() => setOpened(!opened)}
          round
        />
        <MenuItem
          title="Recipes"
          pushPath="/recipes"
          isActive={() => pathname === "/" || pathname === "/recipes"}
        />
        <MenuItem title="New recipe" pushPath="/recipes/add" />
      </aside>
      <div
        className={`menu-overlay${opened ? " menu-overlay--opened" : ""}`}
        onClick={() => setOpened(false)}
      />
    </>
  );
};

export default withRouter(Menu);
