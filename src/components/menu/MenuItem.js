import React from "react";
import { NavLink } from "react-router-dom";

const MenuItem = ({ title, pushPath, isActive }) => (
  <NavLink
    to={pushPath}
    className="menu__item"
    activeClassName="menu__item--active"
    exact
    isActive={isActive}
  >
    {title}
  </NavLink>
);

export default MenuItem;
