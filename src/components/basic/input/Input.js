import React from "react";
import "./styles.sass";

const Input = ({ className, type, label, placeholder, onChange, value }) => {
  return (
    <div className={`input${className ? ` ${className}` : ""}`}>
      {label && <label className="input__label">{label}</label>}
      {type === "text-area" ? (
        <textarea
          className="input__input input__input--textarea"
          value={value}
          placeholder={placeholder}
          onChange={evt => onChange(evt.target.value)}
          rows={3}
        />
      ) : (
        <input
          className="input__input"
          type={type}
          value={value}
          placeholder={placeholder}
          onChange={evt => onChange(evt.target.value)}
        />
      )}
    </div>
  );
};

export default Input;
