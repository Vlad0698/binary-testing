import React from "react";
import "./styles.sass";

const IconButton = ({ icon, round, className, disabled, onClick }) => (
  <button
    className={getClassName(className, round)}
    onClick={onClick}
    disabled={disabled}
  >
    <img className="icon-button__icon" src={icon} alt="icon" />
  </button>
);

const getClassName = (className, round) => {
  const result = `icon-button${className ? ` ${className}` : ""}${
    round ? " icon-button--round" : ""
  }`;
  return result;
};

export default IconButton;
