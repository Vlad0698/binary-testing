import React from "react";
import "./styles.sass";

import Loader from "../loader";
import ErrorMessage from "../error-message";

const Form = ({
  children,
  title,
  loading,
  error,
  onSubmit,
  onTryAgain,
  backButton,
}) => (
  <div className="form">
    {
      <div>
        <h2 className="form__title">{title}</h2>
      </div>
    }
    <form
      className="form__form"
      onSubmit={evt => {
        evt.preventDefault();
        onSubmit();
      }}
    >
      {!loading && <>{children}</>}
      {loading && <Loader className="form__loader" />}
      {error && <ErrorMessage tryAgain={onTryAgain || onSubmit} />}
    </form>
  </div>
);

export default Form;
