import React from "react";
import "./styles.sass";

const Modal = ({ className, toggleModal, children }) => (
  <div className="modal">
    <div className="modal__overlay" onClick={toggleModal} />
    <div className={`modal__content-wrp${className ? ` ${className}` : ""}`}>
      {children}
    </div>
  </div>
);

export default Modal;
