import React from "react";
import "./styles.sass";

const Loader = ({ className }) => (
  <div className={`loader${className ? ` ${className}` : ""}`}>
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default Loader;
