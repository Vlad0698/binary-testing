import React from "react";
import "./styles.sass";

const ErrorMessage = ({ title, message, tryAgain }) => (
  <div className="error-message">
    <h3 className="error-message__title">{title || "Error"}</h3>
    <p className="error-message__message">
      {message || "Oops, something went wrong..."}
    </p>
    {tryAgain && (
      <p
        className="error-message__message error-message__message--link"
        onClick={tryAgain}
      >
        Try again.
      </p>
    )}
  </div>
);

export default ErrorMessage;
