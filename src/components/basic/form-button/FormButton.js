import React from "react";
import "./styles.sass";

const FormButton = ({ title, type, disabled }) => (
  <button className="form-button" type={type || "submit"} disabled={disabled}>
    {title}
  </button>
);

export default FormButton;
