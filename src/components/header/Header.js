import React from "react";
import "./styles.sass";

const Header = props => (
  <header className="header">
    <h1 className="header__title">CookBook</h1>
  </header>
);

export default Header;
