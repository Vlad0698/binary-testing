import React from "react";
import ArrowBack from "../../assets/icons/arrow-back.png";
import Clock from "../../assets/icons/clock--dark.png";

import IconButton from "../basic/icon-button";
import Modal from "../basic/modal";

import moment from "moment";

const HistoryRecord = ({ record: { title, description, date }, setRecord }) => (
  <Modal
    className="recipe-history__detailed-item"
    toggleModal={() => setRecord(null)}
  >
    <div className="recipe-history__detailed-item__header">
      <IconButton
        className="recipe-history__detailed-item__header__back-button"
        onClick={() => setRecord(null)}
        icon={ArrowBack}
        round
      />
      <h3 className="recipe-history__detailed-item__header__title">{title}</h3>
    </div>
    <div className="recipe-history__detailed-item__timestamp">
      <img
        className="recipe-history__detailed-item__timestamp__icon"
        src={Clock}
        alt="clock"
      />
      <p className="recipe-history__detailed-item__timestamp__date">
        {moment(date).format("MMMM Do YYYY, h:mm")}
      </p>
    </div>
    <p className="recipe-history__detailed-item__description">{description}</p>
  </Modal>
);

export default HistoryRecord;
