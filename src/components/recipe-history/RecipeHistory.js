import React, { useState, useEffect } from "react";
import "./styles.sass";

import Loader from "../basic/loader";
import ErrorMessage from "../basic/error-message";
import HistoryRecord from "./HistoryRecord";

import loadHistory from "./loadHistory";
import moment from "moment";
import { withRouter } from "react-router-dom";

const RecipeHistory = ({
  match: {
    params: { recipeId },
  },
}) => {
  const [error, setError] = useState(null);
  const [records, setRecords] = useState(null);
  const [activeRecord, setActiveRecord] = useState(null);

  useEffect(() => {
    if (!recipeId) return;
    loadHistory(recipeId, setRecords, setError);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recipeId]);

  return (
    <div className="recipe-history">
      <h3 className="recipe-history__title">Previous versions of the recipe</h3>
      {records &&
        records.map(item => (
          <p
            className="recipe-history__item"
            onClick={() => setActiveRecord(item)}
          >
            {moment(item.date).format("MMMM Do YYYY, h:mm")}
          </p>
        ))}
      {!records && !error && <Loader className="recipe-history__loader" />}
      {error && <ErrorMessage />}
      {activeRecord && (
        <HistoryRecord record={activeRecord} setRecord={setActiveRecord} />
      )}
    </div>
  );
};

export default withRouter(RecipeHistory);
