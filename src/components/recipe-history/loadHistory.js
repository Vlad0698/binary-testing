import api from "../../services/api";
import { logError } from "../../services/logger";

const loadHistory = async (recipeId, setRecords, setError) => {
  try {
    setError(null);
    const records = await api.request(`/history/${recipeId}`);
    setRecords(records);
    return Promise.resolve();
  } catch (err) {
    setError(logError(err, "load history"));
  }
};

export default loadHistory;
