// LOAD RECIPES
export const loadRecipes = state => ({
  ...state,
  loading: true,
  loadingError: null,
});
export const loadRecipesDone = (state, { recipes }) => ({
  ...state,
  loading: false,
  all: recipes,
});
export const loadRecipesFailed = (state, { err }) => ({
  ...state,
  loading: false,
  loadingError: err,
});

// CREATE RECIPE
export const createRecipe = state => ({
  ...state,
  creating: true,
  creatingError: null,
});
export const createRecipeDone = (state, { recipe }) => ({
  ...state,
  creating: false,
  all: state.all ? [recipe, ...state.all] : null,
});
export const createRecipeFailed = (state, { err }) => ({
  ...state,
  creating: false,
  creatingError: err,
});

// LOAD SINGLE RECIPE
export const loadRecipe = state => ({
  ...state,
  loadingSingle: true,
  loadingSingleError: null,
});
export const loadRecipeDone = (state, { recipe }) => ({
  ...state,
  loadingSingle: false,
  single: recipe,
});
export const loadRecipeFailed = (state, { err }) => ({
  ...state,
  loadingSingle: false,
  loadingSingleError: err,
});

// UPDATE RECIPE
export const updateRecipe = state => ({
  ...state,
  updating: true,
  updatingError: null,
});
export const updateRecipeDone = (state, { recipe }) => ({
  ...state,
  updating: false,
  all: state.all
    ? state.all.map(item => (item._id === recipe._id ? recipe : item))
    : null,
});
export const updateRecipeFailed = (state, { err }) => ({
  ...state,
  updating: false,
  updatingError: err,
});

// DELETE RECIPE
export const deleteRecipe = state => ({
  ...state,
  deleting: true,
  deletingError: null,
});
export const deleteRecipeDone = (state, { id }) => ({
  ...state,
  deleting: false,
  all: state.all ? state.all.filter(item => item._id !== id) : state.all,
});
export const deleteRecipeFailed = (state, { err }) => ({
  ...state,
  deleting: false,
  deletingError: err,
});
