import * as actions from "./actionTypes";

import api from "../../services/api";
import { logError } from "../../services/logger";

export const loadRecipes = () => {
  return async dispatch => {
    dispatch({ type: actions.LOAD_RECIPES });
    try {
      const recipes = await api.request("/recipe");
      dispatch({ type: actions.LOAD_RECIPES_DONE, payload: { recipes } });
    } catch (err) {
      dispatch({
        type: actions.LOAD_RECIPES_FAILED,
        payload: { err: logError(err, "load recipes") },
      });
    }
  };
};

export const createRecipe = data => {
  return async dispatch => {
    dispatch({ type: actions.CREATE_RECIPE });
    try {
      const recipe = await api.request("/recipe", "post", data);
      dispatch({ type: actions.CREATE_RECIPE_DONE, payload: { recipe } });
    } catch (err) {
      dispatch({
        type: actions.CREATE_RECIPE_FAILED,
        payload: { err: logError(err, "create recipe") },
      });
    }
  };
};

export const loadRecipe = id => {
  return async dispatch => {
    dispatch({ type: actions.LOAD_RECIPE });
    try {
      const recipe = await api.request(`/recipe/${id}`);
      dispatch({
        type: actions.LOAD_RECIPE_DONE,
        payload: { recipe },
      });
    } catch (err) {
      dispatch({
        type: actions.LOAD_RECIPE_FAILED,
        payload: { err: logError(err, "load single recipe") },
      });
    }
  };
};

export const updateRecipe = (id, data) => {
  return async dispatch => {
    dispatch({ type: actions.UPDATE_RECIPE });
    try {
      const recipe = await api.request(`/recipe/${id}`, "patch", data);
      dispatch({ type: actions.UPDATE_RECIPE_DONE, payload: { recipe } });
    } catch (err) {
      dispatch({
        type: actions.UPDATE_RECIPE_FAILED,
        payload: { err: logError(err, "update recipe") },
      });
    }
  };
};

export const deleteRecipe = id => {
  return async dispatch => {
    dispatch({ type: actions.DELETE_RECIPE });
    try {
      await api.request(`/recipe/${id}`, "delete");
      dispatch({ type: actions.DELETE_RECIPE_DONE, payload: { id } });
    } catch (err) {
      dispatch({
        type: actions.DELETE_RECIPE_FAILED,
        payload: { err: logError(err, "delete recipe") },
      });
    }
  };
};
