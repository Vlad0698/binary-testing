import * as actions from "./actionTypes";
import * as handlers from "./handlers";

const initialState = {
  all: null,
  loading: true,
  loadingError: null,
  creating: false,
  creatingError: null,
  single: null,
  loadingSingle: false,
  loadingSingleError: null,
  updating: false,
  updatingError: null,
  deleting: false,
  deletingError: null,
};

const recipes = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.LOAD_RECIPES:
      return handlers.loadRecipes(state);
    case actions.LOAD_RECIPES_DONE:
      return handlers.loadRecipesDone(state, payload);
    case actions.LOAD_RECIPES_FAILED:
      return handlers.loadRecipesFailed(state, payload);
    case actions.CREATE_RECIPE:
      return handlers.createRecipe(state);
    case actions.CREATE_RECIPE_DONE:
      return handlers.createRecipeDone(state, payload);
    case actions.CREATE_RECIPE_FAILED:
      return handlers.createRecipeFailed(state, payload);
    case actions.LOAD_RECIPE:
      return handlers.loadRecipe(state);
    case actions.LOAD_RECIPE_DONE:
      return handlers.loadRecipeDone(state, payload);
    case actions.LOAD_RECIPE_FAILED:
      return handlers.loadRecipeFailed(state, payload);
    case actions.UPDATE_RECIPE:
      return handlers.updateRecipe(state);
    case actions.UPDATE_RECIPE_DONE:
      return handlers.updateRecipeDone(state, payload);
    case actions.UPDATE_RECIPE_FAILED:
      return handlers.updateRecipeFailed(state, payload);
    case actions.DELETE_RECIPE:
      return handlers.deleteRecipe(state);
    case actions.DELETE_RECIPE_DONE:
      return handlers.deleteRecipeDone(state, payload);
    case actions.DELETE_RECIPE_FAILED:
      return handlers.deleteRecipeFailed(state, payload);
    default:
      return state;
  }
};

export default recipes;
