import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import { env } from "../config";

const store =
  env === "dev"
    ? createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))
    : createStore(reducers, applyMiddleware(thunk));

export default store;
