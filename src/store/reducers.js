import { combineReducers } from "redux";
import recipes from "./recipes/reducer";

const reducers = combineReducers({
  recipes,
});

export default reducers;
