import React from "react";
import Clock from "../../assets/icons/clock.png";
import moment from "moment";

const Recipe = ({ recipe: { _id, title, updatedAt }, history }) => (
  <div
    className="recipes-list__item"
    onClick={() => history.push(`/recipe/${_id}`)}
  >
    <p className="recipes-list__item__title">{title}</p>
    <div className="recipes-list__item__date">
      <p className="recipes-list__item__date__text">
        {moment(updatedAt).format("MMMM Do YYYY, h:mm")}
      </p>
      <img
        className="recipes-list__item__date__clock-icon"
        src={Clock}
        alt="clock"
      />
    </div>
  </div>
);

export default Recipe;
