import React, { useEffect } from "react";
import "./styles.sass";

import Loader from "../../components/basic/loader";
import ErrorMessage from "../../components/basic/error-message";
import Recipe from "./Recipe";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { mapStateToProps, mapDispatchToProps } from "./container";

const RecipesList = ({ history, recipes, loading, error, loadRecipes }) => {
  useEffect(() => {
    if (!recipes) loadRecipes();
  }, [recipes, loadRecipes]);

  return (
    <div className="recipes-list">
      {!loading &&
        recipes &&
        recipes.map(item => (
          <Recipe key={item._id} recipe={item} history={history} />
        ))}
      {loading && <Loader />}
      {(!recipes || !recipes.length) && !error && !loading && (
        <EmptyMessage history={history} />
      )}
      {error && !loading && <ErrorMessage tryAgain={loadRecipes} />}
    </div>
  );
};

const EmptyMessage = ({ history }) => (
  <p className="recipes-list__empty-message">
    No recipies yet!{" "}
    <span
      className="recipes-list__empty-message__link"
      onClick={() => history.push("/recipes/add")}
    >
      Add your first recipe.
    </span>
  </p>
);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RecipesList)
);
