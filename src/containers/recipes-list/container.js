import { loadRecipes } from "../../store/recipes/actions";

export const mapStateToProps = ({
  recipes: { all, loading, loadingError },
}) => ({
  recipes: all,
  error: loadingError,
  loading,
});

export const mapDispatchToProps = { loadRecipes };
