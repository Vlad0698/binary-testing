import { updateRecipe, loadRecipe } from "../../../store/recipes/actions";

export const mapStateToProps = (
  {
    recipes: {
      all,
      single,
      updating,
      updatingError,
      loadingSingle,
      loadingSingleError,
    },
  },
  {
    match: {
      params: { recipeId },
    },
  }
) => {
  const foundRecipe = all ? all.find(item => item._id === recipeId) : null;
  const recipe = foundRecipe || single;
  const error = loadingSingleError
    ? "loading"
    : updatingError
    ? "updating"
    : null;
  return {
    recipe,
    error,
    loading: updating || loadingSingle,
  };
};

export const mapDispatchToProps = { updateRecipe, loadRecipe };
