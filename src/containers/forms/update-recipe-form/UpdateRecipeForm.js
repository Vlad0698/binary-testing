import React, { useState, useEffect } from "react";

import Form from "../../../components/basic/form";
import FormButton from "../../../components/basic/form-button";
import Input from "../../../components/basic/input";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { mapStateToProps, mapDispatchToProps } from "./container";

const UpdateRecipeForm = ({
  recipe,
  history,
  match: {
    params: { recipeId },
  },
  updateRecipe,
  loadRecipe,
  loading,
  error,
}) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [input, setInput] = useState(true);

  useEffect(() => {
    if (!loading && !error && !input) history.push("/recipes");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (!recipe) loadRecipe(recipeId);
    if (recipe) {
      setTitle(recipe.title);
      setDescription(recipe.description);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recipe]);

  return (
    <Form
      title="Update recipe"
      onSubmit={() => {
        updateRecipe(recipeId, { title, description });
        setInput(false);
      }}
      loading={loading}
      error={error && !input ? error : null}
      onTryAgain={
        error === "loading"
          ? () => loadRecipe(recipeId)
          : error === "updating"
          ? () => updateRecipe(recipeId, { title, description })
          : undefined
      }
      backButton
    >
      <Input
        label="Title"
        placeholder="Recipe title"
        type="text"
        value={title}
        onChange={setTitle}
      />
      <Input
        label="Description"
        placeholder="Recipe description"
        type="text-area"
        value={description}
        onChange={setDescription}
      />
      <FormButton
        title="Update recipe"
        disabled={
          loading ||
          !recipe ||
          (recipe.title === title && recipe.description === description)
        }
      />
    </Form>
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UpdateRecipeForm)
);
