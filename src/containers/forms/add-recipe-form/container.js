import { createRecipe } from "../../../store/recipes/actions";

export const mapStateToProps = ({ recipes: { creating, creatingError } }) => ({
  error: creatingError,
  loading: creating,
});

export const mapDispatchToProps = { createRecipe };
