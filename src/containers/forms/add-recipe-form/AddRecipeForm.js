import React, { useState, useEffect } from "react";

import Form from "../../../components/basic/form";
import FormButton from "../../../components/basic/form-button";
import Input from "../../../components/basic/input";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { mapStateToProps, mapDispatchToProps } from "./container";

const AddRecipeForm = ({ history, createRecipe, loading, error }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [input, setInput] = useState(true);

  useEffect(() => {
    if (!loading && !error && !input) history.push("/recipes");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  return (
    <Form
      title="New recipe"
      onSubmit={() => {
        createRecipe({ title, description });
        setInput(false);
      }}
      loading={loading}
      error={error && !input ? error : null}
    >
      <Input
        label="Title"
        placeholder="Recipe title"
        type="text"
        value={title}
        onChange={setTitle}
      />
      <Input
        label="Description"
        placeholder="Recipe description"
        type="text-area"
        value={description}
        onChange={setDescription}
      />
      <FormButton
        title="Add recipe"
        disabled={!title.length || !description.length || loading}
      />
    </Form>
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddRecipeForm)
);
