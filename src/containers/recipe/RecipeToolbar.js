import React from "react";
import "./styles.sass";

import Edit from "../../assets/icons/edit.png";
import Trash from "../../assets/icons/trash.png";
import Clock from "../../assets/icons/clock--dark.png";
import moment from "moment";

import IconButton from "../../components/basic/icon-button";

const RecipeToolbar = ({ recipe, onEditClick, onDeleteClick }) => (
  <div className="recipe__toolbar">
    <div className="recipe__toolbar__buttons">
      <IconButton
        className="recipe__toolbar__buttons__item"
        icon={Edit}
        onClick={onEditClick}
        disabled={!recipe}
      />
      <IconButton
        className="recipe__toolbar__buttons__item"
        icon={Trash}
        onClick={onDeleteClick}
        disabled={!recipe}
      />
    </div>
    {recipe && (
      <div className="recipe__toolbar__date">
        <p className="recipe__toolbar__date__text">
          {moment(recipe.createdAt).format("MMMM Do YYYY, h:mm")}
        </p>
        <img className="recipe__toolbar__date__icon" src={Clock} alt="clock" />
      </div>
    )}
  </div>
);

export default RecipeToolbar;
