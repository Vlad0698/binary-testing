/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import "./styles.sass";

import Loader from "../../components/basic/loader";
import ErrorMessage from "../../components/basic/error-message";
import RecipeToolbar from "./RecipeToolbar";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { mapStateToProps, mapDispatchToProps } from "./container";

const Recipe = ({
  match: {
    params: { recipeId },
  },
  history,
  recipe,
  loading,
  error,
  loadRecipe,
  deleteRecipe,
}) => {
  const [beforeDelete, setBeforeDelete] = useState(true);

  useEffect(() => {
    if (!recipe || recipe._id !== recipeId) loadRecipe(recipeId);
  }, []);

  useEffect(() => {
    if (!loading && !beforeDelete) history.push("/recipes");
  }, [loading]);

  return (
    <div className="recipe">
      <RecipeToolbar
        recipe={recipe}
        onEditClick={() => history.push(`/recipe/${recipeId}/update`)}
        onDeleteClick={() => {
          setBeforeDelete(false);
          deleteRecipe(recipeId);
        }}
      />
      {recipe && (
        <>
          <h3 className="recipe__title">{recipe.title}</h3>
          <p className="recipe__description">{recipe.description}</p>
        </>
      )}
      {loading && <Loader className="recipe__loader" />}
      {error && (
        <ErrorMessage
          tryAgain={
            error === "loading"
              ? () => loadRecipe(recipeId)
              : error === "deleting"
              ? () => deleteRecipe(recipeId)
              : undefined
          }
        />
      )}
    </div>
  );
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Recipe)
);
