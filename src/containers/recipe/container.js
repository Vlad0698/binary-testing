import { loadRecipe, deleteRecipe } from "../../store/recipes/actions";

export const mapStateToProps = (
  {
    recipes: {
      single,
      all,
      loadingSingle,
      loadingSingleError,
      deleting,
      deletingError,
    },
  },
  {
    match: {
      params: { recipeId },
    },
  }
) => {
  const foundRecipe = all ? all.find(item => item._id === recipeId) : null;
  const recipe = foundRecipe || single;
  const error = loadingSingleError
    ? "loading"
    : deletingError
    ? "deleting"
    : null;
  return { recipe, loading: loadingSingle || deleting, error };
};

export const mapDispatchToProps = {
  loadRecipe,
  deleteRecipe,
};
