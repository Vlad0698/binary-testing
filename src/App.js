import React from "react";
import "./styles/App.sass";
import "./styles/reset.sass";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

import Header from "./components/header";
import Menu from "./components/menu";
import pages from "./pages";

const App = props => (
  <Provider store={store}>
    <Router>
      <div className="page">
        <Header />
        <Menu />
        <main className="page__content">
          <Switch>
            <Route
              exact
              path={["/", "/recipes"]}
              component={pages.RecipesPage}
            />
            <Route exact path="/recipes/add" component={pages.AddRecipePage} />
            <Route
              exact
              path="/recipe/:recipeId"
              component={pages.SingleRecipePage}
            />
            <Route
              exact
              path="/recipe/:recipeId/update"
              component={pages.UpdateRecipePage}
            />
          </Switch>
        </main>
      </div>
    </Router>
  </Provider>
);

export default App;
